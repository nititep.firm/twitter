const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
// DATABASE CONNECTED
const mysql2 = require("mysql2/promise");
const dbConfig = require("./config/db");
const pool = mysql2.createPool(dbConfig);
// =================================== //

const makeTweetCtrl = require("./ctrl/tweet");
const tweetRepo = require("./repo/tweet");
const tweetCtrl = makeTweetCtrl(pool, tweetRepo);

const makeUserCtrl = require("./ctrl/user");
const userRepo = require("./repo/user");
const userCtrl = makeUserCtrl(pool, userRepo);

const makePollCtrl = require("./ctrl/poll");
const pollRepo = require("./repo/poll");
const pollCtrl = makePollCtrl(pool, pollRepo);

const makeNotiCtrl = require("./ctrl/noti");
const notiRepo = require("./repo/noti");
const notiCtrl = makeNotiCtrl(pool, notiRepo);

const router = new Router()
  // .get('/todo', tweetCtrl.list)
  // Auth
  .post("/auth/signup", authCtrl.signUp)
  .post("/auth/signin", authCtrl.signIn)
  .get("/auth/signout", authCtrl.sinOut)
  .post("auth/verify", authCtrl.verify)

  .post("/user", userCtrl.create)
  .patch("/user/photo/:id", userCtrl.changePhoto)
  .patch("/user/cover/:id", userCtrl.changeCover)
  .put("/user/follow", userCtrl.follow)
  .delete("/user/unfollow", userCtrl.unfollow)
  .put("/user/chat", userCtrl.chat)

  .get("/tweet/:user/:start/:limit/: ", userCtrl.signUp)
  
  .post("/tweet", tweetCtrl.create)
  .post("/tweet/addphoto", tweetCtrl.addPhoto)
  .put("/tweet/like", tweetCtrl.like)
  .delete("/tweet/unlike", tweetCtrl.unlike)
  .post("tweet/hashtag", tweetCtrl.hashTag)
  .post("/retweet", tweetCtrl.retweet)
  .put("/reply", tweetCtrl.reply)

  .post("/poll", pollCtrl.create)
  .put("/poll/addchoice", pollCtrl.addChoice)
  .put("/poll/vote", pollCtrl.vote)

  .post("/noti", notiCtrl.create)
  .put("/noti/:id/markread", notiCtrl.markAsRead);

// .get('/todo/:id', tweetCtrl.get)
// .patch('/todo/:id', tweetCtrl.update)
// .delete('/todo/:id', tweetCtrl.removeTodo)
// .put('/todo/:id/complete', tweetCtrl.complete)
// .delete('/todo/:id/complete', tweetCtrl.incomplete)

const app = new Koa();

app.use(bodyParser());
app.use(router.routes());
app.listen(3000);
