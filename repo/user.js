module.exports = {
    create,
    patchPhoto,
    patchCover,
    doFollow,
    doUnfollow,
    doChat,
    doSignUp   
  }
  
  async function create (db, user) {
    const result = await db.execute(`
      insert into users (
        username, email, password, name, status
      ) values (
        ?, ?, ?, ?, ?
      )
    `, [
      user.username, user.email, user.password,
      user.name, 0
    ])
    return result[0].insertId
  }

  function createEntity(row) {
    return {
      id: row.id,
      username: row.username,
      email: row.email,
      password: row.password,
      status: row.status,
      name: row.name,
      photo: row.photo,
      cover: row.cover
    }
  }

  async function list(db) {
    let [rows] = await db.execute(`select * from users`)
    return rows.map(createEntity)
  }

  async function find(db, id) {
    let [rows] = await db.execute(`select * from users where id = ?`, [id])
    return createEntity(rows[0])
  }
  
  async function patchPhoto (db, userPhoto, id) {
    return await db.execute(`
      update users set
        photo = ?
      where id = ?
    `, [userPhoto, id])
  }
  
  async function patchCover (db, userCover, id) {
    await db.execute(`
      update users set
        cover = ?
      where id = ?
    `, [userCover, id])
  }
  
  async function doFollow (db, follow) {
    const result = await db.execute(`
      insert into follows (
        follower_id, following_id
      ) values (
        ?, ?
      )
    `, [follow.follower_id, follow.following_id])
    return result[0].insertId
  }
  
  async function doUnfollow (db, user) {
    await db.execute(`
      delete from follows
      where follower_id = ? and following_id = ?
    `, [user.follower_id, user.following_id])
  }
  
  async function doChat (db, user){
    const result = await db.execute(`
      insert into user_chats (sender_id, receiver_id, content, type) 
      values (?, ?, ?, ?)
    `, [
      user.sender_id, user.receiver_id, user.content, user.type
    ])
    return result[0].insertId
  }

  async function doSignUp (db, sign){
    const result = await db.execute(`
    insert into users (username, email, password, name, location, bio, birth_date_d, birth_date_m, birth_date_y) 
    values (?, ?, ?, ?, ?, ?, ?, ?, ?)
  `, [
    sign.username, sign.email, sign.password, sign.name, sign.location, sign.bio, sign.birth_date_d, sign.birth_date_m, sign.birth_date_y
  ])
  return result[0].insertId
  }

  
  
  
  
  // function createEntity(row) {
  //     return {
  //       id: row.id,
  //       username: row.username,
  //       password: row.password
  //     };
  //   }
  
  //   async function find(db, id) {
  //     const [rows] = await db.execute(
  //       `select id, username, password from test where id = ?`,
  //       [id]
  //     );
  //     return createEntity(rows[0]);
  //   }
  
  // //   function remove() {
  // //       return await db.execute(`delete from test where id = ?`, [id]
  // //     );      
  // //   }
  
  //   module.exports = {
  //     find,
  //     remove  
  //   };